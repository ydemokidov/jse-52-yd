package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class TaskUnbindFromProjectResponse extends AbstractResultResponse {

    public TaskUnbindFromProjectResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public TaskUnbindFromProjectResponse() {
    }

}
