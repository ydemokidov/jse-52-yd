package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.dto.response.project.*;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull IConnectionProvider connectionProvider) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectChangeStatusByIdRequest request);

    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectChangeStatusByIndexRequest request);

    @WebMethod
    ProjectClearResponse clearProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectClearRequest request);

    @WebMethod
    ProjectCompleteByIdResponse completeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCompleteByIdRequest request);

    @WebMethod
    ProjectCompleteByIndexResponse completeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCompleteByIndexRequest request);

    @WebMethod
    ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCreateRequest request);

    @WebMethod
    ProjectListResponse listProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectListRequest request);

    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectRemoveByIdRequest request);

    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectRemoveByIndexRequest request);

    @WebMethod
    ProjectShowByIdResponse showProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectShowByIdRequest request);

    @WebMethod
    ProjectShowByIndexResponse showProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectShowByIndexRequest request);

    @WebMethod
    ProjectStartByIdResponse startProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectStartByIdRequest request);

    @WebMethod
    ProjectStartByIndexResponse startProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectStartByIndexRequest request);

    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectUpdateByIdRequest request);

    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectUpdateByIndexRequest request);

}