package com.t1.yd.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command " + command + " is not supported...");
    }

}
