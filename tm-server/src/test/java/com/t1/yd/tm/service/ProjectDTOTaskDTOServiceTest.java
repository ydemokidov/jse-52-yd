package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.dto.IDtoProjectTaskService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.marker.UnitCategory;
import com.t1.yd.tm.service.dto.ProjectDtoService;
import com.t1.yd.tm.service.dto.ProjectTaskDtoService;
import com.t1.yd.tm.service.dto.TaskDtoService;
import com.t1.yd.tm.service.dto.UserDtoService;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.USER_1_PROJECT_DTO_1;
import static com.t1.yd.tm.constant.TaskTestData.USER_1_PROJECT_1_TASK_DTO_1;
import static com.t1.yd.tm.constant.UserTestData.ALL_USER_DTOS;
import static com.t1.yd.tm.constant.UserTestData.USER_DTO_1;

@Category(UnitCategory.class)
public class ProjectDTOTaskDTOServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ILoggerService loggerService = new LoggerService(connectionService);

    private IProjectDtoService projectDtoService = new ProjectDtoService(connectionService, loggerService);

    private ITaskDtoService taskDtoService = new TaskDtoService(connectionService, loggerService);

    private IDtoProjectTaskService service = new ProjectTaskDtoService(connectionService);

    private IUserDtoService userDtoService = new UserDtoService(connectionService, propertyService, loggerService);

    @Before
    public void initRepository() {
        taskDtoService.clear();
        projectDtoService.clear();
        userDtoService.clear();

        userDtoService.add(ALL_USER_DTOS);
    }

    @Test
    public void bindTaskToProject() {
        projectDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        @NotNull final TaskDTO bindedTaskDTO = taskDtoService.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId());
        Assert.assertEquals(USER_1_PROJECT_DTO_1.getId(), bindedTaskDTO.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        service.unbindTaskFromProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        Assert.assertTrue(taskDtoService.findAllByProjectId(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).isEmpty());
    }

    @Test
    public void removeProjectById() {
        projectDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        service.removeProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        Assert.assertTrue(taskDtoService.findAllByProjectId(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).isEmpty());
    }

    @After
    public void clearData() {
        taskDtoService.clear();
        projectDtoService.clear();
        userDtoService.clear();
    }

}