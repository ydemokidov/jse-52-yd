package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoProjectRepository;
import com.t1.yd.tm.dto.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public class ProjectDtoRepository extends AbstractDtoUserOwnedRepository<ProjectDTO> implements IDtoProjectRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}
