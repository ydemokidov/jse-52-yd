package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoSessionRepository;
import com.t1.yd.tm.dto.model.SessionDTO;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public class SessionDtoRepository extends AbstractDtoUserOwnedRepository<SessionDTO> implements IDtoSessionRepository {

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }
}
