package com.t1.yd.tm.api.service;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(MessageListener messageListener);

}
