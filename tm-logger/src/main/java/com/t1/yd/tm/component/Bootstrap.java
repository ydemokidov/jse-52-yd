package com.t1.yd.tm.component;

import com.t1.yd.tm.api.service.IReceiverService;
import com.t1.yd.tm.listener.LoggerListener;
import com.t1.yd.tm.service.ReceiverService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;

public class Bootstrap {

    public void init() {
        @NotNull final ActiveMQConnectionFactory activeMqConnectionFactory = new ActiveMQConnectionFactory("failover://tcp://127.0.0.1:61616");
        activeMqConnectionFactory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(activeMqConnectionFactory);
        receiverService.receive(new LoggerListener());
    }

}
