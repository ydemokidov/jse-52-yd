package com.t1.yd.tm.command.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.dto.model.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpointClient();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return getServiceLocator().getAuthEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(@NotNull final UserDTO userDTO) {
        System.out.println("Id: " + userDTO.getId());
        System.out.println("Login: " + userDTO.getLogin());
        System.out.println("Email: " + userDTO.getEmail());
    }

}
